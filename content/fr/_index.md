---
title: "Je m'appelle Lenny."

description: "Un développeur passioné."
cascade:
  featured_image: 'https://doc.casthighlight.com/wp-content/uploads/2019/06/luxfon.com-18586.jpg'
---

Bonjour, je m'appelle Lenny et je fais des applications client et serveur performantes et respectant les derniers standarts technologique. Comprenant l'importance des données utilisateurs, je m'engage à sécuriser les infrastructures que je conçois.